package com.eureka.client2.infrastructure.repository;

import com.eureka.client2.entities.Client2;
import javassist.NotFoundException;
import java.util.List;

public interface Client2Port {

    Client2 create(Client2 client2);

    void delete(String client2Id) throws NotFoundException;

    List<Client2> findAll();

    Client2 findById(String id) throws NotFoundException;
}
