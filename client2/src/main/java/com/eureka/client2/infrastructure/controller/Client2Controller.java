package com.eureka.client2.infrastructure.controller;

import com.eureka.client2.entities.Client2;
import com.eureka.client2.infrastructure.repository.Client2Port;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v0/")
public class Client2Controller {

    private final Client2Port client2Port;

    @PostMapping("client2")
    public ResponseEntity<Client2> create(@RequestBody Client2 client2) {

        Client2 client = client2Port.create(client2);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) throws NotFoundException {

        client2Port.delete(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Client2>> findAll() {

        List<Client2> clients = client2Port.findAll();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<Client2> findById(@PathVariable("id") String id) throws NotFoundException {

        Client2 client = client2Port.findById(id);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }
}
