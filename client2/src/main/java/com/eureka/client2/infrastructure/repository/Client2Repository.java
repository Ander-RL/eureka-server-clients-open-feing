package com.eureka.client2.infrastructure.repository;

import com.eureka.client2.entities.Client2;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Repository;
import java.util.List;

@Data
@AllArgsConstructor
@Repository
public class Client2Repository implements Client2Port {

    Client2RepositoryJpa repository;

    @Override
    public Client2 create(Client2 client2) {
        repository.save(client2);
        return client2;
    }

    @Override
    public void delete(String client2Id) throws NotFoundException {
        Client2 client = repository.findById(client2Id).orElseThrow(() -> new NotFoundException("No se encuentra"));
        repository.delete(client);
    }

    @Override
    public List<Client2> findAll() {
        return repository.findAll();
    }

    @Override
    public Client2 findById(String id) throws NotFoundException {
        return repository.findById(id).orElseThrow(() -> new NotFoundException("No se encuentra"));
    }
}
