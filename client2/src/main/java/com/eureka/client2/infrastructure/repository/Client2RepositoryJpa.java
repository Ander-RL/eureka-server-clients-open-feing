package com.eureka.client2.infrastructure.repository;

import com.eureka.client2.entities.Client2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Client2RepositoryJpa extends JpaRepository<Client2, String> {
}
