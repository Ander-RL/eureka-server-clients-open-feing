package com.eureka.client1.infrastructure;

import com.eureka.client1.entities.Client2Feign;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="client2")
public interface Client1ServiceProxySimple {
    @GetMapping("api/v0/{id_client2}")
    Client2Feign findById(@PathVariable("id_client2") String client2Id);
}
