package com.eureka.client1.infrastructure.repository;

import com.eureka.client1.entities.Client1;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@AllArgsConstructor
@Repository
public class Client1Repository implements Client1Port {

    Client1RepositoryJpa repository;

    @Override
    public Client1 create(Client1 client1) {
        repository.save(client1);
        return client1;
    }

    @Override
    public void delete(Integer client1Id) throws NotFoundException {
        Client1 client = repository.findById(client1Id).orElseThrow(() -> new NotFoundException("No se encuentra"));
        repository.delete(client);
    }

    @Override
    public List<Client1> findAll() {
        return repository.findAll();
    }
}
