package com.eureka.client1.infrastructure.repository;

import com.eureka.client1.entities.Client1;
import javassist.NotFoundException;

import java.util.List;

public interface Client1Port {
    Client1 create(Client1 client1);
    void delete(Integer client1Id) throws NotFoundException;
    List<Client1> findAll();
}
