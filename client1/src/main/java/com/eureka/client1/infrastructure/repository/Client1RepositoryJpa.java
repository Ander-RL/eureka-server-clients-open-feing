package com.eureka.client1.infrastructure.repository;

import com.eureka.client1.entities.Client1;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Client1RepositoryJpa extends JpaRepository<Client1, Integer> {
}
