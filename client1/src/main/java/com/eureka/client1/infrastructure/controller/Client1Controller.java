package com.eureka.client1.infrastructure.controller;

import com.eureka.client1.entities.Client1;
import com.eureka.client1.entities.Client2Feign;
import com.eureka.client1.infrastructure.Client1ServiceProxySimple;
import com.eureka.client1.infrastructure.repository.Client1Port;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@AllArgsConstructor
@RestController
@RequestMapping("/api/v0/")
public class Client1Controller {

    private final Client1Port client1Port;
    private final Client1ServiceProxySimple proxySimple;

    @PostMapping("client1")
    public ResponseEntity<Client1> create(@RequestBody Client1 client1) {

        Client1 client = client1Port.create(client1);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Integer id) throws NotFoundException {

        client1Port.delete(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Client1>> findAll() {

        List<Client1> clients = client1Port.findAll();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @GetMapping("/feign/{id_client2}")
    public Client2Feign finById(@PathVariable("id_client2") String client2Id) {
        return proxySimple.findById(client2Id);
    }
}
